/**
 * @author J. Daza
 * @description Prueba de desarrollo backend
 */

const { Product, CarInsurance } = require('./coTest')
const Products = require('./db/products.json')

const productsAtDayZero = []
Products.forEach(({ name, sellIn, price }) =>
  productsAtDayZero.push(new Product(name, sellIn, price))
)

const carInsurance = new CarInsurance(productsAtDayZero)

for (let i = 1; i <= 30; i += 1) {
  console.log(`Day ${i}`)
  console.table(carInsurance.updatePrice())
  console.log('')
}
