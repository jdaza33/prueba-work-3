class Product {
  constructor(name, sellIn, price) {
    this.name = name
    this.sellIn = sellIn
    this.price = price
  }
}

class CarInsurance {
  constructor(products = []) {
    this.products = products
  }

  updatePrice() {
    for (let i in this.products) {
      let { name, sellIn, price } = this.products[i]
      if (/Special/i.test(name))
        price =
          sellIn <= 5 && sellIn > 0
            ? price + 3
            : sellIn <= 10 && sellIn > 5
            ? price + 2
            : sellIn <= 0
            ? 0
            : price + 1
      else if (/Full/i.test(name)) price = sellIn <= 0 ? price + 2 : price + 1
      else if (/Super/i.test(name)) price = sellIn <= 0 ? price - 4 : price - 2
      else if (!/Mega/i.test(name)) price = sellIn <= 0 ? price - 2 : price - 1

      if (!/Mega/i.test(name)) {
        sellIn -= 1
        price = price < 0 ? 0 : price > 50 ? 50 : price
      }

      this.products[i] = { name, sellIn, price }
    }

    return this.products
  }
}

module.exports = {
  Product,
  CarInsurance,
}
